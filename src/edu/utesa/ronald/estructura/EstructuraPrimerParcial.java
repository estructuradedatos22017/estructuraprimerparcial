package edu.utesa.ronald.estructura;

import edu.utesa.ronald.estructura.tareas.archivos.Articulo;
import edu.utesa.ronald.estructura.test.ListaEnlazada;
import edu.utesa.ronald.estructura.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * clase main
 * Created by ronald on 6/22/17.
 */
public class EstructuraPrimerParcial {


    public static void main(String[] args) {

        String [] entradas = Constants.get().readText(Constants.FILE).split("\n");
        Lista<Articulo> articuloListaCopy = new Lista<>();
        for (String entrada : entradas) {
            String[] entradaArticulo = entrada.split(";");
            Nodo<Articulo> articuloNodo = new Nodo<>();
            Articulo articulo = new Articulo();
            articulo.setReferencia(entradaArticulo[0]);
            articulo.setDescripcion(entradaArticulo[1]);
            articulo.setPrecio(new Double(entradaArticulo[2]));
            articulo.setItbis(new Long(entradaArticulo[3]));
            articulo.setCantidad(new Integer(entradaArticulo[4]));
            articulo.setCategoria(entradaArticulo[5]);
            articuloNodo.setDato(articulo);
            articuloListaCopy.add(articuloNodo);
        }
        articuloListaCopy.print();

//
//        Lista<Articulo> lista1 = new Lista<>();
//        for (int i = 0; i < 10; i++) {
//            Nodo<Articulo> articuloNodo = new Nodo<>();
//            SecureRandom secureRandom = new SecureRandom();
//            Integer ref = secureRandom.nextInt(10);
//            Long itbis = 18L;
//            Integer cantidad = ref + 10;
//            articuloNodo.setDato(new Articulo("AR" + ref, "lapiz", (double) secureRandom.nextInt(30), itbis, cantidad, "escuela"));
//            lista1.add(articuloNodo);
//        }
//        lista1.print();
//        System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
//        System.out.println("Referencia");
//        articuloListaCopy.filtrar(articulo -> articulo.getReferencia().contains("AR")).print();
//        System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
//        System.out.println("Desc");
//        articuloListaCopy.filtrar(articulo -> articulo.getDescripcion().contains("p")).print();
//        System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
//        System.out.println("CAt");
//        articuloListaCopy.filtrar(articulo -> articulo.getCategoria().contains("AR1")).print();
//        System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
//        System.out.println("PRecio");
//        articuloListaCopy.filtrar(articulo -> articulo.getPrecio() >= 10 && articulo.getPrecio() <= 40).print();
//        System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
//        System.out.println("cantidad");
//        articuloListaCopy.filtrar(articulo -> articulo.getCantidad() >= 15 && articulo.getCantidad() <= 20).print();
//
//        startMenu();

    }

    private static void startMenu() {
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;
        System.out.println("\tMENU PRINCIPAL\n");
        System.out.println("1 = Ver Lista de nodo");
        System.out.println("2 = Insertar un nodo");
        System.out.println("3 = Buscar un nodo");
        System.out.println("4 = Modificar un nodo");
        System.out.println("5 = Eliminar un nodo");
        System.out.println("6||> = Salir");
        String opcion = scanner.nextLine();
        while (!exit) {
            System.out.println("\tMENU PRINCIPAL\n");
            System.out.println("1 = Ver Lista de nodo");
            System.out.println("2 = Insertar un nodo");
            System.out.println("3 = Buscar un nodo");
            System.out.println("4 = Modificar un nodo");
            System.out.println("5 = Eliminar un nodo");
            System.out.println("6||> = Salir");
            opcion = scanner.nextLine();
            switch (opcion) {
                case "1":
                    System.out.println("\tLista de nodo\n");
//                    list.print();
                    break;
                case "2":
                    System.out.println("Ingrese un nodo nuevo");
//                    list.addprimero(opcion);
                    break;
                case "3":
                    System.out.println("Ingrese un nodo a buscar");
//                    list.buscarNodo(opcion);
                    break;
                case "4":
                    System.out.println("Ingrese un nodo a modificar");
//                    list.modificarnodo(opcion);
                    break;
                case "5":
                    System.out.println("Ingrese un nodo a eliminar");
//                    list.eliminarnodo(opcion);
                    break;
                case "6":
                    exit = true;
                    break;
            }
        }
    }


    private static void test0() {
        //        Lista lista = new Lista(0);
//        Lista r = new Lista(0);
//        for (int i = 65; i < 95; i++) {
//            Nodo<Character> nodo = new Nodo<>();
//            nodo.setDato(((char) i));
//            lista.add(nodo);
//
//            nodo = new Nodo<>();
//            nodo.setDato((char) i);
//            r.addHead(nodo);
//        }
//        System.out.println("R:");
//        r.print();
//        System.out.println("edu.utesa.ronald.estructura.Lista:");
//        lista.print();

    }

    private static void test() {

        //Creo la lista enlazada de numeros
        //Puede ser de String, double, Objetos, etc.
        ListaEnlazada<Integer> lista = new ListaEnlazada<>();

        System.out.println("Insercion de numeros del 0 al 9 en forma de cola");
        for (int i = 0; i < 10; i++) {
            lista.insertarUltimo(i);
        }

        //Mostramos la lista
        lista.mostrar();

        System.out.println("");
        System.out.println("Numero de elementos: " + lista.cuantosElementos());
        System.out.println("");

        System.out.println("Eliminación del dato que esta en la posicion 3");
        lista.borraPosicion(3); //Elimina el el dato 3

        lista.mostrar();

        System.out.println("Numero de elementos: " + lista.cuantosElementos());
        System.out.println("");

        System.out.println("Insercion del dato 2 en la posicion 5");
        lista.introducirDato(5, 2);

        lista.mostrar();

        System.out.println("Numero de elementos: " + lista.cuantosElementos());
        System.out.println("");

        System.out.println("Modificamos el dato de la posicion 5 por un 3");
        lista.modificarDato(5, 3);

        lista.mostrar();

        System.out.println("Numero de elementos: " + lista.cuantosElementos());
        System.out.println("");

        System.out.println("Inserto en la posicion 0");
        lista.introducirDato(0, 10);

        lista.mostrar();
        System.out.println("");

        System.out.println("Inserto en la ultima posicion");
        //Equivalente a insertarUltimo
        lista.introducirDato(lista.cuantosElementos(), 11);

        lista.mostrar();
        System.out.println("");

        System.out.println("Posicion del dato 5: " + lista.indexOf(5));
        System.out.println("Posicion del dato 5 desde la posicion 7: " + lista.indexOf(5, 7));

        System.out.println("");

        System.out.println("¿Existe el dato 10 en la lista? " + lista.datoExistente(10));
        System.out.println("¿Existe el dato 20 en la lista? " + lista.datoExistente(20));
    }

}
