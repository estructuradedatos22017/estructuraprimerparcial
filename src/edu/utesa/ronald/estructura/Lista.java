package edu.utesa.ronald.estructura;

import java.util.function.Predicate;

/**
 * edu.utesa.ronald.estructura.Lista
 * Created by ronald on 6/22/17.
 */
public class Lista<T> {

    private Nodo<T> first = null;
    private int cant_nodos = 0;

    public Lista() {

    }

    public Lista(Nodo<T> first) {
        this.first = first;
        first.setDato(null);
    }

    public Lista(int cant_nodos) {
        for (int j = 0; j < cant_nodos; j++) {
            Nodo<T> nodo = new Nodo<>();
            add(nodo);
        }
    }

    public void add(Nodo<T> nodo) {
        if (first == null) {
            first = nodo;
        } else {
            Nodo<T> last;
            for (last = first; last.getNext() != null; last = last.getNext()) ;
            last.setNext(nodo);
        }
    }

    public int getCant_nodos() {
        return cant_nodos;
    }

    public void addHead(Nodo<T> nodo, int i) {
        if (first == null) {
            first = nodo;
        } else {
            nodo.setNext(first);
            first = nodo;
        }
    }

    public void addHead(Nodo<T> nodo) {
        if (first == null) {
            first = nodo;
        } else {
            nodo.setNext(first);
            first = nodo;
            first.getNext().setBefore(first);
        }
    }

    /*
    *La comparcion se hace dependiendo el criterio de el objeto que le paso
    *
    */
    public void insertNode(Nodo<T> nodo, int a) {
        Nodo<T> last = first;
        while (last != null) {
            if (Double.parseDouble(nodo.getDato().toString()) >= Double.parseDouble(last.getDato().toString())) {
                last = last.getNext();
            } else {
                break;
            }
        }
        if (last == null) {
            add(nodo);
        } else {
            if (Double.parseDouble(nodo.getDato().toString()) < Double.parseDouble(first.getDato().toString())) {
                addHead(nodo);
            } else {
                nodo.setNext(last);
                Nodo<T> k;
                for (k = first; k.getNext() != last; k = k.getNext()) ;
                k.setNext(nodo);
            }
        }
    }

    /*
    *La comparcion se hace dependiendo el criterio de el objeto que le paso
    *
    */
    public void insertNode(Nodo<T> nodo) {
        Nodo<T> last = first;
        while (last != null) {
            if (Double.parseDouble(nodo.getDato().toString()) >= Double.parseDouble(last.getDato().toString())) {
                last = last.getNext();
            } else {
                break;
            }
        }
        if (last == null) {
            add(nodo);
        } else {
            if (Double.parseDouble(nodo.getDato().toString()) < Double.parseDouble(first.getDato().toString())) {
                addHead(nodo);
            } else {
                nodo.setNext(last);
                nodo.setBefore(last.getBefore());
                last.setBefore(nodo);
                nodo.getBefore().setNext(nodo);
                nodo.getNext().setBefore(nodo.getBefore());
                nodo.getBefore().setNext(nodo.getNext());
                Nodo<T> k;
                for (k = first; k.getNext() != last; k = k.getNext()) ;
                k.setNext(nodo);
            }
        }
    }

    public void borrar(Nodo<T> nodo) {
        Nodo<T> last;
        if (first == nodo) {
            first = first.getNext();
            nodo.setNext(null);
        } else {
            for (last = first; last.getNext() != nodo; last = last.getNext()) ;
            last.setNext(nodo.getNext());
        }
    }

    public Lista<T> filtrar(Predicate<T> predicate) {
        Lista<T> listaFiltrada = new Lista<>();
        for (Nodo<T> l = first; l != null; l = l.getNext()) {
            if (predicate.test(l.getDato())) {
                listaFiltrada.add(new Nodo<>(l.getDato()));
            }
        }
        return listaFiltrada;
    }

    public void print() {
        for (Nodo<T> l = first; l != null; l = l.getNext()) {
            System.out.println(l.getDato());
        }
    }


}
