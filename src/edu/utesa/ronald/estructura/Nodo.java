package edu.utesa.ronald.estructura;

/**
 * Elemento de la lista enlazada
 * Created by ronald on 6/22/17.
 */
public class Nodo<T> {

    private Nodo<T> next;
    private Nodo<T> before;
    private T dato;

    public Nodo() {
    }

    public Nodo(T dato) {
        this.dato = dato;
    }

    public Nodo<T> getNext() {
        return next;
    }

    public void setNext(Nodo<T> next) {
        this.next = next;
    }

    public Nodo<T> getBefore() {
        return before;
    }

    public void setBefore(Nodo<T> before) {
        this.before = before;
    }

    public T getDato() {
        return dato;
    }

    public void setDato(T dato) {
        this.dato = dato;
    }
}
