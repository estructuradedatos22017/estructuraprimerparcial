package edu.utesa.ronald.estructura.parcial.segundo;

import edu.utesa.ronald.estructura.Nodo;

/**
 * Created by ronald on 7/11/17.
 */
public interface StackMethods<T> {

    Nodo<T> pop();

    Nodo<T> peek();

    Nodo<T> push();
}
