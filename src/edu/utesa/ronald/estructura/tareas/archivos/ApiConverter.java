package edu.utesa.ronald.estructura.tareas.archivos;

/**
 * Created by ronald on 7/4/17.
 */
public interface ApiConverter<T> {

    T toApi();
}
