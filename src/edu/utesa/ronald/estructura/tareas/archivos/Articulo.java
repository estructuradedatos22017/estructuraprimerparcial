package edu.utesa.ronald.estructura.tareas.archivos;

import edu.utesa.ronald.estructura.utils.Constants;

/**
 * Created by ronald on 7/4/17.
 */
public class Articulo implements ApiConverter<ArticuloApi> {

    private String referencia;
    private String descripcion;
    private Double precio;
    private Long itbis;
    private Integer cantidad;
    private String categoria;

    public Articulo() {
    }

    public Articulo(String referencia, String descripcion, Double precio, Long itbis, Integer cantidad, String categoria) {
        this.referencia = referencia;
        this.descripcion = descripcion;
        this.precio = precio;
        this.itbis = itbis;
        this.cantidad = cantidad;
        this.categoria = categoria;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Long getItbis() {
        return itbis;
    }

    public void setItbis(Long itbis) {
        this.itbis = itbis;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }


    @Override
    public String toString() {
        return referencia + Constants.FIELD_SEPARATOR + descripcion + Constants.FIELD_SEPARATOR +
                precio + Constants.FIELD_SEPARATOR + itbis + Constants.FIELD_SEPARATOR + cantidad +
                Constants.FIELD_SEPARATOR + categoria;
    }

    public void split(String frame) {
        String[] data = splitFrame(frame);
        if (data.length > 0 && data.length == 6) {
            try {
                referencia = data[0];
                descripcion = data[1];
                precio = new Double(data[2]);
                itbis = new Long(data[3]);
                cantidad = new Integer(data[4]);
                categoria = data[5];
                System.out.println("ss");
            } catch (Exception ignored) {
                System.out.println("fallo");
            }
        } else {
            System.out.println("fallo");
        }
    }

    protected String[] splitFrame(String frame) {
        return splitFrame(Constants.FIELD_SEPARATOR, frame);
    }

    protected String[] splitFrame(String regex, String frame) {
        try {
            return frame.split(regex);
        } catch (Exception ignored) {
            System.out.println(ignored);
            return new String[0];
        }
    }


    @Override
    public ArticuloApi toApi() {
        return null;
    }
}
