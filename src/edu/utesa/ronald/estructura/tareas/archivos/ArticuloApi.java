package edu.utesa.ronald.estructura.tareas.archivos;

/**
 * Created by ronald on 7/4/17.
 */
public class ArticuloApi {
    private String referencia;
    private String descripcion;
    private String precio;
    private String itbis;
    private String catidad;
    private String categoria;

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getItbis() {
        return itbis;
    }

    public void setItbis(String itbis) {
        this.itbis = itbis;
    }

    public String getCatidad() {
        return catidad;
    }

    public void setCatidad(String catidad) {
        this.catidad = catidad;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
