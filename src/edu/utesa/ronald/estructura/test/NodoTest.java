package edu.utesa.ronald.estructura.test;

/**
 * Created by ronald on 7/12/17.
 */

public class NodoTest<T> {

    private T dato;
    private NodoTest<T> siguiente;

    /**
     * Constructor por defecto
     */
    public NodoTest() {
        siguiente = null;
    }

    /**
     * Le pasamos un dato al nodo
     *
     * @param p
     */
    public NodoTest(T p) {
        siguiente = null;
        dato = p;
    }

    /**
     * Le pasamos un dato y su siguiente nodo al nodo
     *
     * @param t         Dato a insertar
     * @param siguiente Su sisguiente nodo
     */
    public NodoTest(T t, NodoTest<T> siguiente) {
        this.siguiente = siguiente;
        dato = t;
    }

    public T getDato() {
        return dato;
    }

    public void setDato(T dato) {
        this.dato = dato;
    }

    public NodoTest<T> getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoTest<T> siguiente) {
        this.siguiente = siguiente;
    }

}