package edu.utesa.ronald.estructura.utils;

import edu.utesa.ronald.estructura.Lista;
import edu.utesa.ronald.estructura.tareas.archivos.Articulo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Singleton para los metodos que van a ser comunes y constantes.
 * Created by ronald on 7/11/17.
 */
public class Constants {

    public static final String FIELD_SEPARATOR = ";";
    public static final String EXTENSION = ".txt";
    public static final String FILE = "Datafile";


    public static final Lista<Articulo> ARTICULO_LISTA = new Lista<>();

    private static Constants instance = null;

    private Constants() {
    }

    public static synchronized Constants get() {
        if (instance == null) {
            instance = new Constants();
        }
        return instance;
    }

    public String[] basicFrame(String frame) {
        try {
            int playStart = frame.indexOf(FIELD_SEPARATOR);
            String generalInfoFrameSection = frame.substring(0, playStart);
            return split(frame.substring(playStart + 1));
        } catch (Exception ignored) {
            return new String[]{};
        }
    }

    public String[] split(String frame) {
        return frame.split(FIELD_SEPARATOR);
    }

    public String readText(String fileName) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(fileName + EXTENSION)));
            StringBuilder builder = new StringBuilder();
            String aux;
            while ((aux = bufferedReader.readLine()) != null) {
                builder.append(aux+"\n");
            }
            return builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
    public List<String> readAllLines(String fileName) {
        List<String> lines = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(fileName + EXTENSION)));
            String aux;

            while ((aux = bufferedReader.readLine()) != null) {
                lines.add(aux);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return lines;
    }
}
