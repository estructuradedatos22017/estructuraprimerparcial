package edu.utesa.ronald.estructura.utils;

import edu.utesa.ronald.estructura.Lista;
import edu.utesa.ronald.estructura.tareas.archivos.Articulo;

/**
 * Numeros Random.
 * <p>
 * Created by ronald on 2/10/17.
 */
public class Data {
    private String name;
    private Lista<Articulo> articuloLista;

    public Data(String name, Lista<Articulo> articuloLista) {
        this.name = name;
        this.articuloLista = articuloLista;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Lista<Articulo> getArticuloLista() {
        return articuloLista;
    }

    public void setArticuloLista(Lista<Articulo> articuloLista) {
        this.articuloLista = articuloLista;
    }
}
