package edu.utesa.ronald.estructura.utils;

import edu.utesa.ronald.estructura.Lista;
import edu.utesa.ronald.estructura.Nodo;
import edu.utesa.ronald.estructura.tareas.archivos.Articulo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.security.SecureRandom;

/**
 * Generador de data para los test.
 * <p>
 * Created by ronald on 2/10/17.
 */
public class DataGenerator {

    private static String[] print = new String[]{"A,B,C,D,E,F,G,M,H,I,J"};

    public static void generate(String fileName, int cant) {
        Lista<Articulo> lista1 = new Lista<>();
        try {
            PrintWriter printWriter = new PrintWriter(new File(fileName + Constants.EXTENSION));
            for (int i = 0; i < cant; i++) {
                Nodo<Articulo> articuloNodo = new Nodo<>();

                SecureRandom secureRandom = new SecureRandom();
                SecureRandom secureRandom2 = new SecureRandom();
                Integer ref = secureRandom.nextInt(9999);
                Integer letra = secureRandom2.nextInt(100);
                Integer cantidad = ref + 10;
                Long itbis = 18L;
                articuloNodo.setDato(new Articulo("AR" + ref, "lapiz", (double) secureRandom.nextInt(30), itbis, cantidad, "escuela"));

                lista1.add(articuloNodo);
                lista1.print();
                printWriter.print(articuloNodo.getDato().toString() + "\n");
            }
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        DataGenerator.generate(Constants.FILE, 10);
    }
}
